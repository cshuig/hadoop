package com.cshuig.hadoop.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by hogan on 2015/10/26.
 */
public class HadoopHdfs01Test {

    private static final String HDFS_URL = "hdfs://hadoopip:9000";
    private static final String LOCAL_STORE = "D://hadoop//download";
    private FileSystem fileSystem;

    @Before
    public void setup() throws URISyntaxException, IOException, InterruptedException {
        // 如果没有指定第三个参数, 那么会经常出现 权限不足 的异常
        fileSystem = FileSystem.get(new URI(HDFS_URL), new Configuration(), "root");
    }

    /**
     * 从hdfs下载一个文件
     * @throws IOException
     */
    @Test
    public void getHDFSFile() throws IOException {
        InputStream inputStream = fileSystem.open(new Path("/jdk"));
        OutputStream outputStream = new FileOutputStream(LOCAL_STORE + "/hadoop2.2.0-64bit.tar.gz");
        IOUtils.copyBytes(inputStream, outputStream, 4096, true);
    }

    @Test
    public void copyToLocalFile() throws IOException {
        // 测试发现，如果不加第一个和最后一个参数，这个方法会报 java.lang.NullPointerException
//        fileSystem.copyToLocalFile(false, new Path("/book/AngularJS权威教程.pdf"), new Path(LOCAL_STORE + "/AngularJS权威教程.pdf"), true);
        fileSystem.copyToLocalFile(false, new Path("/sum_info_result/part-r-00000"), new Path(LOCAL_STORE + "/part-r-00000.txt"), true);
    }

    /**
     * 上传文件到hdfs
     * @throws IOException
     */
    @Test
    public void updateFileToHDFS() throws IOException {
        OutputStream outputStream = fileSystem.create(new Path("/book/AngularJS权威教程.pdf"));
        InputStream inputStream = new FileInputStream("C://Users//hogan//Desktop//book//AngularJS权威教程.pdf");
        IOUtils.copyBytes(inputStream, outputStream, 4096, true);
    }

    @Test
    public void deleteHDFSFile() throws IOException {
        boolean flag = fileSystem.delete(new Path("/book/AngularJS权威教程.pdf"), false);
        if (flag) {
            System.out.println("删除成功");
        }
    }
}
