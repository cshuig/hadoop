package com.cshuig.hadoop.mapreduce.wordcount;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/27.
 */
public class WCReducer extends Reducer<Text, LongWritable, Text, LongWritable> {
    /**
     * 对map的结果进行合并
     * reduce的结果: hello 5
     * @param key
     * @param values
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        long count = 0;
        for (LongWritable value : values) {
            count = count + value.get();
        }
        context.write(key, new LongWritable(count));
    }
}
