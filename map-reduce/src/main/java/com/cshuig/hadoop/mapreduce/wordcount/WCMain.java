package com.cshuig.hadoop.mapreduce.wordcount;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/27.
 */
public class WCMain {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance();

        /** ********* 非常重要: 设置main方法所在的类 ********* */
        job.setJarByClass(WCMain.class);

        /** *********设置 Mapper 相关属性 ********* */
        job.setMapperClass(WCMapper.class);   //设置自定义Mapper类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);
        //设置map的输入数据,可以从指定多个文件, 文件是hdfs里面的
        //FileInputFormat.addInputPath(job, new Path("/words"));
        FileInputFormat.setInputPaths(job, new Path("/words"));

        /** *********设置 Reducer 相关属性 ********* */
        job.setReducerClass(WCReducer.class); //设置自定义的Reducer类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        //设置reducer的输出数据, 文件输出到hdfs里面
        FileOutputFormat.setOutputPath(job, new Path("/word_counts"));

        // 提交job,可以使用submit(不好), 使用waitForCompletion可以打印出进度和详情
        job.waitForCompletion(true);
    }
}
