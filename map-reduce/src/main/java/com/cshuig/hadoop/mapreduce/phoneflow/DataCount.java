package com.cshuig.hadoop.mapreduce.phoneflow;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * 用来存储手机号码对应的上行和下行流量
 * Created by hogan on 2015/10/28.
 * hadoop对象必须要实现Writable, hadoop才能对其进行序列化和反序列化
 */
public class DataCount implements Writable {

    private String phoneNo;     //手机号码
    private long upPayLoad;     //上行流量
    private long downPayLoad;   //下行流量
    private long totalLoad;     //总流量

    /** 如果类带有有参构造函数, 那么必须显示的设置一个无参构造函数, 这样Mapreduce方法才能创建对象，然后通过readFields方法从序列化的数据流中读出进行赋值 **/
    public DataCount() {
    }

    public DataCount(String phoneNo, long upPayLoad, long downPayLoad, long totalLoad) {
        this.phoneNo = phoneNo;
        this.upPayLoad = upPayLoad;
        this.downPayLoad = downPayLoad;
        this.totalLoad = totalLoad;
    }

    /**
     * 序列化到输出流
     * @param out
     * @throws IOException
     */
    public void write(DataOutput out) throws IOException {
        out.writeUTF(phoneNo);
        out.writeLong(upPayLoad);
        out.writeLong(downPayLoad);
        out.writeLong(totalLoad);
    }

    /**
     * 从输入流中反序列化
     * 注意顺序
     * @param in
     * @throws IOException
     */
    public void readFields(DataInput in) throws IOException {
        phoneNo = in.readUTF();
        upPayLoad = in.readLong();
        downPayLoad = in.readLong();
        totalLoad = in.readLong();
    }


    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public long getUpPayLoad() {
        return upPayLoad;
    }

    public void setUpPayLoad(long upPayLoad) {
        this.upPayLoad = upPayLoad;
    }

    public long getDownPayLoad() {
        return downPayLoad;
    }

    public void setDownPayLoad(long downPayLoad) {
        this.downPayLoad = downPayLoad;
    }

    public long getTotalLoad() {
        return totalLoad;
    }

    public void setTotalLoad(long totalLoad) {
        this.totalLoad = totalLoad;
    }

    @Override
    public String toString() {
        return "DataCount{" +
                "phoneNo='" + phoneNo + '\'' +
                ", upPayLoad=" + upPayLoad +
                ", downPayLoad=" + downPayLoad +
                ", totalLoad=" + totalLoad +
                '}';
    }
}
