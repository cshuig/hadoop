package com.cshuig.hadoop.mapreduce.phoneflow;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/28.
 */
public class CountReducer extends Reducer<Text, DataCount, Text, DataCount> {

    /**
     * 将mapper的结果再做聚合处理, 结果示例:
     * 13480253104     DataCount{phoneNo='13480253104', upPayLoad=180, downPayLoad=180, totalLoad=360}
     * @param key
     * @param values
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<DataCount> values, Context context) throws IOException, InterruptedException {
        long upPayLoad = 0;
        long downPayLoad = 0;
        long totalLoad = 0;
        for (DataCount dataCount : values) {
            upPayLoad += dataCount.getUpPayLoad();
            downPayLoad += dataCount.getDownPayLoad();
            totalLoad += dataCount.getTotalLoad();
        }
        context.write(key, new DataCount(key.toString(), upPayLoad, downPayLoad, totalLoad));
    }
}
