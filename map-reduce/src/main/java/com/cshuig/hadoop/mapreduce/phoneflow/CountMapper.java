package com.cshuig.hadoop.mapreduce.phoneflow;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/28.
 */
public class CountMapper extends Mapper<LongWritable, Text, Text, DataCount> {
    /**
     * map的结果示例
     * {
     *     1363157985066: {new DataCount(), new DataCount(), new DataCount(), ...}
     *     1363157995052: {new DataCount(), new DataCount(), new DataCount(), ...}
     * }
     * 后期再聚合处理是交给 reduce
     * @param key
     * @param value
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] line = value.toString().split("\t");   //每行中的元素用一个tab分割
        String phoneNo = line[1];
        long upPayLoad = Long.valueOf(line[8]);
        long downPayLoad = Long.valueOf(line[9]);
        context.write(new Text(phoneNo), new DataCount(phoneNo, upPayLoad, downPayLoad, upPayLoad + downPayLoad));
    }
}
