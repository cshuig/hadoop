package com.cshuig.hadoop.mapreduce.wordcount;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

/**
 * Created by hogan on 2015/10/27.
 * 由于jdk自带的序列化能力差, 所以hadoop自己实现了一套带有序列化的基本类
 */
public class WCMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
    /**
     * 场景例子: 就像是选班长一样, 每次从投票箱中获取一张投票, 然后获取名字, 名字出现一次 记一个1, 最后会自动统计求和
     * 每次执行传进来的key和value，可以看成是 投票箱中的一张投票纸
     * map的结果示例: hello [1,1,1,1,1]
     * @param key
     * @param value
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 接收v1数据 --- 一行数据
        String line = value.toString();
        // 切分数据(拆分当前投票纸中的名字), 用空格分割
        String[] words = line.split(" ");
        for (String word : words) {
            // 单词出现一次, 就记一个1
            context.write(new Text(word), new LongWritable(1));
        }
    }
}
