package com.cshuig.hadoop.combiner;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by hogan on 2015/11/2.
 */
public class WordPathCountReducer extends Reducer<Text, Text, Text, Text> {

    private Text v = new Text();

    /**
     *
     * @param key 格式就是一个单词 如 hello
     * @param values 每个Text格式如 a.txt->5  --- 代表key在这个文件中出现的次数
     * @param context 输出格式: hello {"a.txt->5", "b.txt->2"}, 汇总出key在每个文件中出现的个数
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        StringBuilder stringBuilder = new StringBuilder();
        for (Text value : values) {
            stringBuilder.append(value.toString()).append("\t");
        }
        v.set(stringBuilder.toString());
        context.write(key, v);
    }
}
