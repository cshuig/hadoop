package com.cshuig.hadoop.combiner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Created by hogan on 2015/11/2.
 */
public class AppMain {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance(new Configuration());
        job.setJarByClass(AppMain.class);

        job.setMapperClass(WordPathCountMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        FileInputFormat.setInputPaths(job, new Path("/inverted_input_data"));//hdfs次目录下面有多个文件

        job.setCombinerClass(WordPathCountCombiner.class);

        job.setReducerClass(WordPathCountReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputKeyClass(Text.class);
        FileOutputFormat.setOutputPath(job, new Path("/word_path_count"));

        job.waitForCompletion(true);
    }
}
