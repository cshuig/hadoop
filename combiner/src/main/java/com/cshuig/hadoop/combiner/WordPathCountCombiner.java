package com.cshuig.hadoop.combiner;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by hogan on 2015/11/2.
 */
public class WordPathCountCombiner extends Reducer<Text, Text, Text, Text> {
    private Text k = new Text();
    private Text v = new Text();

    /**
     * @param key 格式如 hello->a.txt
     * @param values 每个text都是1
     * @param context 输出格式如: key是hello  value是a.txt->5
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        // key格式如 hello->a.txt
        String[] keys = key.toString().split("->");
        String word = keys[0];
        String path = keys[1];
        long count = 0;
        for (Text value : values) {
            count += Long.parseLong(value.toString());
        }
        k.set(word);
        v.set(path + "->" + count);
        context.write(k, v);
    }
}
