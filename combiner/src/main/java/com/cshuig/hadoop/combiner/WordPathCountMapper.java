package com.cshuig.hadoop.combiner;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;

/**
 * Created by hogan on 2015/11/2.
 */
public class WordPathCountMapper extends Mapper<LongWritable, Text, Text, Text> {

    private Text k = new Text();
    private Text v = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] line = value.toString().split(" ");
        FileSplit fileSplit =  (FileSplit)context.getInputSplit();
        String path = fileSplit.getPath().toString();
        for (String word : line) {
            k.set(word + "->" + path);
            v.set("1");
            context.write(k, v);
        }
    }
}
