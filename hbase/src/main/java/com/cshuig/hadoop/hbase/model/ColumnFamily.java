package com.cshuig.hadoop.hbase.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hogan on 2015/11/30.
 * 列族
 */
public class ColumnFamily {
    private List<ColumnFamily> list = new ArrayList<>();
    private String name;
    private Integer versions = 1;

    public static class InstanceHolder {
        public static final ColumnFamily columnFamily = new ColumnFamily();
    }

    private ColumnFamily() {
    }

    private ColumnFamily(String name) {
        this.name = name;
    }

    private ColumnFamily(String name, Integer versions) {
        this.name = name;
        this.versions = versions;
    }

    public ColumnFamily set(String name) {
        list.add(new ColumnFamily(name));
        return this;
    }

    public ColumnFamily set(String name, Integer versions) {
        list.add(new ColumnFamily(name, versions));
        return this;
    }

    public List<ColumnFamily> getList() {
        return list;
    }

    public String getName() {
        return name;
    }

    public Integer getVersions() {
        return versions;
    }
}
