package com.cshuig.hadoop.sort;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/30.
 */
public class SumMapper extends Mapper<LongWritable, Text, Text, InforBean> {
    private InforBean inforBean = new InforBean();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] v1 = line.split("\t");
        inforBean.set(v1[0], Double.valueOf(v1[1]), Double.valueOf(v1[2]));
        context.write(new Text(v1[0]), inforBean);
    }
}
