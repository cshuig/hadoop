package com.cshuig.hadoop.sort;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/30.
 * 排序是针对InforBean进行的, 所有key2必须是InforBean, v2没东西输出就用null
 */
public class SortMapper extends Mapper<LongWritable, Text, InforBean, NullWritable> {
    private InforBean inforBean = new InforBean();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = key.toString();
        String[] v1 = line.split("\t");
        inforBean.set(v1[0], Double.valueOf(v1[1]), Double.valueOf(v1[2]));
        context.write(inforBean, NullWritable.get());
    }
}
