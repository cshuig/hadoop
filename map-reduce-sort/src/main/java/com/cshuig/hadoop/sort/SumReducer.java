package com.cshuig.hadoop.sort;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/30.
 */
public class SumReducer extends Reducer<Text, InforBean, Text, InforBean> {
    private InforBean inforBean = new InforBean();
    @Override
    protected void reduce(Text key, Iterable<InforBean> values, Context context) throws IOException, InterruptedException {
        double income = 0;
        double expense = 0;
        for (InforBean i : values) {
            income += i.getIncome();
            expense += i.getExpense();
        }
        inforBean.set(key.toString(), income, expense);
        context.write(key, inforBean);
    }
}
