package com.cshuig.hadoop.sort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/30.
 */
public class SortMain {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance(new Configuration());
        job.setJarByClass(SortMain.class);

        job.setMapperClass(SortMapper.class);
        job.setMapOutputKeyClass(InforBean.class);
        job.setMapOutputValueClass(NullWritable.class);
        FileInputFormat.setInputPaths(job, new Path(PathConstants.SORT_MAPPER_INPUT));

        job.setReducerClass(SortReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(InforBean.class);
        FileOutputFormat.setOutputPath(job, new Path(PathConstants.SORT_REDUCER_RESULT));

        job.waitForCompletion(true);

    }
}
