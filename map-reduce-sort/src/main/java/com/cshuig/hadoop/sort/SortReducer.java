package com.cshuig.hadoop.sort;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


/**
 * Created by hogan on 2015/10/30.
 */
public class SortReducer extends Reducer<InforBean, NullWritable, Text, InforBean> {

    private Text key = new Text();
    @Override
    protected void reduce(InforBean value, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        key.set(value.getAccount());
        context.write(key, value);
    }
}
