package com.cshuig.hadoop.sort;

/**
 * Created by hogan on 2015/10/30.
 */
public class PathConstants {
    public static final String SUM_MAPPER_INPUT = "/sort_info.txt";
    public static final String SUM_REDUCER_RESULT = "/sum_info_result";

    public static final String SORT_MAPPER_INPUT = SUM_REDUCER_RESULT;
    public static final String SORT_REDUCER_RESULT = "/sort_info_result";
}
