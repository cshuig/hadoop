package com.cshuig.hadoop.sort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * Created by hogan on 2015/10/30.
 */
public class SumMain {
    public static void main( String[] args ) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance(new Configuration());
        job.setJarByClass(SumMain.class);

        job.setMapperClass(SumMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(InforBean.class);
        FileInputFormat.setInputPaths(job, new Path(PathConstants.SUM_MAPPER_INPUT));

        job.setReducerClass(SumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(InforBean.class);
        FileOutputFormat.setOutputPath(job, new Path(PathConstants.SUM_REDUCER_RESULT));

        job.waitForCompletion(true);
    }
}
