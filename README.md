### 项目顺序
* 1、hdfs
* 2、map-reduce
* 3、map-reduce-sort
* 4、combiner

### 1、hdf
* 使用java api 操作hdfs

### 2、map-reduce
* 手机号码流量统计, 且加入了Partitioner(分区)将Mapper分发到不同的reducer处理, 结果产生多个分区文件
* 单词个数统计

### 3、map-reduce-sort
* 使用两个mapReduce进行关联统计(目前第二个mapreduce还有问题,在获取第一个mapreduce产生的hdfs文件时候会报: 数组下标越界问题)

### 4、combiner
* 使用 Mapper ---> Combiner ---> Reducer 进行统计每个单词在不同的文件中各自出现的次数(倒排索引)